var expect = require("chai").expect;
var request = require("request");

describe("hello world", function() {
  var res = {};
  before(function(done) {
    request.get("https://www.google.com/", function(err, response) {
      res = response;
      done();
    });
  });

  it("should be able to make a passing expectation", function() {
    expect(res.statusCode).to.equal(200)
  });
  it("should also be able to make a failing test", function() {
    expect(10).to.equal(2);
  });
});